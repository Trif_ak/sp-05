package ru.trifonov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.dto.ProjectDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectEndpoint {
    @WebMethod
    void insertProject(
            @WebParam(name = "projectDTO", partName = "projectDTO") @NotNull ProjectDTO projectDTO
    );

    @WebMethod
    void updateProject(
            @WebParam(name = "projectDTO", partName = "projectDTO") @NotNull ProjectDTO projectDTO
    );

    @WebMethod
    ProjectDTO getProject(
            @WebParam(name = "id", partName = "id") @NotNull String id
    );

    @WebMethod
    void deleteProject(
            @WebParam(name = "id", partName = "id") @NotNull String id
    );

    @WebMethod
    List<ProjectDTO> getAllProject();
}
