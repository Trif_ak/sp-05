package ru.trifonov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.trifonov.tm.api.ITaskService;
import ru.trifonov.tm.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Component
@NoArgsConstructor
@WebService(endpointInterface = "ru.trifonov.tm.endpoint.ITaskEndpoint")
public final class TaskEndpoint implements ITaskEndpoint {
    @NotNull
    ITaskService taskService;

    @Autowired
    public void setTaskService(@NotNull final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    @WebMethod
    public void initTask(
            @WebParam(name = "taskDTO", partName = "taskDTO") @NotNull TaskDTO taskDTO
    ) {
        taskService.insert(taskDTO);
    }

    @Override
    @WebMethod
    public void updateTask(
            @WebParam(name = "taskDTO", partName = "taskDTO") @NotNull TaskDTO taskDTO
    ) {
        taskService.update(taskDTO);
    }

    @Override
    @WebMethod
    public TaskDTO getTask(
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) {
        return taskService.find(id);
    }

    @Override
    @WebMethod
    public List<TaskDTO> getAllTaskOfProject(
            @WebParam(name = "projectId", partName = "projectId") @NotNull final String projectId
    ) {
        return taskService.findAllByProjectId(projectId);
    }

    @Override
    @WebMethod
    public void deleteTask(
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws Exception { ;
        taskService.delete(id);
    }

    @Override
    @WebMethod
    public List<TaskDTO> getAllTask() {
        return taskService.findAll();
    }
}
