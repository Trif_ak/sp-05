package ru.trifonov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITaskEndpoint {
    @WebMethod
    void initTask(
            @WebParam(name = "taskDTO", partName = "taskDTO") @NotNull TaskDTO taskDTO
    );

    @WebMethod
    void updateTask(
            @WebParam(name = "taskDTO", partName = "taskDTO") @NotNull TaskDTO taskDTO
    );

    @WebMethod
    TaskDTO getTask(
            @WebParam(name = "id", partName = "id") @NotNull String id
    );

    @WebMethod
    List<TaskDTO> getAllTaskOfProject(
            @WebParam(name = "projectId", partName = "projectId") @NotNull String projectId
    );

    @WebMethod
    void deleteTask(
            @WebParam(name = "id", partName = "id") @NotNull String id
    ) throws Exception;

    @WebMethod
    List<TaskDTO> getAllTask();
}
