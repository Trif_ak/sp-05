package ru.trifonov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import ru.trifonov.tm.api.IProjectService;
import ru.trifonov.tm.dto.ProjectDTO;

import javax.annotation.PostConstruct;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Component
@WebService(endpointInterface = "ru.trifonov.tm.endpoint.IProjectEndpoint")
public final class ProjectEndpoint implements IProjectEndpoint {
    @NotNull
    IProjectService projectService;

    @Autowired
    public void setProjectService(@NotNull final IProjectService projectService) {
        this.projectService = projectService;
    }

    @PostConstruct
    public void init() {
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    }

    @Override
    @WebMethod
    public void insertProject(
            @WebParam(name = "projectDTO", partName = "projectDTO") @NotNull final ProjectDTO projectDTO
    ) {
        projectService.insert(projectDTO);
    }

    @Override
    @WebMethod
    public void updateProject(
            @WebParam(name = "projectDTO", partName = "projectDTO") @NotNull final ProjectDTO projectDTO
    ) {
        projectService.update(projectDTO);
    }

    @Override
    @WebMethod
    public ProjectDTO getProject(
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) {
        return projectService.find(id);
    }

    @Override
    @WebMethod
    public void deleteProject(
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) {
        projectService.delete(id);
    }

    @Override
    @WebMethod
    public List<ProjectDTO> getAllProject() {
        return projectService.findAll();
    }
}