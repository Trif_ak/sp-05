package ru.trifonov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.enumerate.CurrentStatus;
import ru.trifonov.tm.util.IdUtil;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractDTO implements Serializable {
    @NotNull
    protected String id = IdUtil.getUUID();
    @NotNull
    protected String projectId = "";

    @NotNull
    protected String name = "";

    @NotNull
    protected String description = "";

    @NotNull
    protected CurrentStatus status = CurrentStatus.PLANNED;

    @NotNull
    protected Date beginDate = new Date();

    @NotNull
    protected Date endDate = new Date();

    @NotNull
    protected Date createDate = new Date();
}
