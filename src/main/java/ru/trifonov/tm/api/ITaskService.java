package ru.trifonov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.dto.TaskDTO;
import ru.trifonov.tm.model.Task;

import java.util.List;

public interface ITaskService {
    void insert(@Nullable TaskDTO taskDTO);

    void update(@Nullable TaskDTO taskDTO);

    @NotNull List<TaskDTO> findAll();

    @NotNull List<TaskDTO> findAllByProjectId(@NotNull String projectId);

    @NotNull TaskDTO find(@Nullable String id);

    void delete(@Nullable String id);

    @NotNull TaskDTO entityToDTO(@NotNull Task task);

    @NotNull Task dtoToEntity(@NotNull TaskDTO taskDTO);
}
